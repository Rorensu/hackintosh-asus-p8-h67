# Clover - EFI *Sandy bridge*
 - **CPU** : i5-2400
 - **GPU** : RX 580
 - **Motherboard** : ASUS P8H67
 
 

## Outils

 - [Hackintool](https://github.com/headkaze/Hackintool)
 - [Clover Configurator](https://mackie100projects.altervista.org/download-clover-configurator/)
 
 ## Kext depôt
 
 - [Lilu](https://github.com/acidanthera/Lilu)
 - [Weg](https://github.com/acidanthera/WhateverGreen)
 

 
